# ToDo App

## Context

Everybody need things to do. Specially when you are a person with not a good memory. Until now, you have learned awesome features from React.js and we are going to use them in order to complete this little app.

## The Assignment

Create a To Do application.

### Setup Instructions

```sh
#(1) Clone the project
git clone https://gitlab.com/agzeri/react-todo-app todoapp

#(2) Go to the project
cd todoapp/starter-code

#(3) Install npm packages
npm install

#(4) Start the server
npm start
```

### Deliverables

+ A GitHub repository

## Requeriments

For this app you can write as many components you need or work in just one.

+ App
  + Sidebar
    + Search
    + Buttons
  + Content
    + Form
    + Tasks

![Components](./components.png)

### Sprint 1 | Basic Project

Create the basic project in React and make sure that it looks good.

### Sprint 2 | Date

Show the correct date.

Use `Date` object from JavaScript native to get the current date.

+ [Date](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)

**We are NOT using `moment.js` to do this.**

### Sprint 3 | Create New Task

Create a basic form with an `input` and `button`. You’ll need to save that information in the State, and for each task you’ll need an `id`, `name`, `completed`.

Remember that each new task should be uncompleted, so make sure `completed` is `false`.


```js
// Task object
{
  id: ?,
  name: "...",
  completed: false
}
```

### Sprint 4 | List All Tasks

Iterate over your State and print all Tasks in the DOM. For each record you will need to add a `checkbox`.

### Sprint 5 | Check for Completed Tasks

Work with events on _checkboxes_. If you click in an uncompleted task you should grab that id, find it on the list, and change its value (completed) from false to true.

Also, as a way to give feedback to the user for completed tasks, I recommend to add an opacity of 50% to the element.

_You may take advantage from `Object.assign()` and probably you will need to merge that new task into the State property you handle_.

### Sprint 6 | Filter by “All”, “Complete” and “Incomplete”

It’s time to filter our tasks, if you click on any of three buttons you should see Tasks related to that filter.

### Sprint 7 | Search by

What about implementing a search? Yes! Filter tasks by name acoording to the value typed in the search box.

### Sprint 8 | Validate Inputs

Your app should not be able to save a task without a name. Handle in the State the error and show it as a feedback.

### Sprint 9 | BONUS | Saving twice

Don’t let the user save two tasks with the same name.

### Sprint 10 | BONUS | Edit Task Name

Ups! I made a mistake writing my Task. Can I change the name?

### Sprint 11 | BONUS | Remove Task from List

I’m not more interested in saving tasks after completed, I want to remove them.

### Sprint 12 | BONUS | Add “Date” to each Task

You learnt how to handle dates, add a new input to save the date you want to finish your task. And display it next to Task name.

### Sprint 13 | BONUS | Mark as Uncompleted

If you have a task checked as done, you can also mark as uncompleted.

## Expected Results

### Normal Mode

![ToDo Demo](TodoLab.mov)

#### About the Design

All `CSS` is already included. Make sure you follow the structure to create the needed look and feel.

###### HTML code

```html
<!-- App -->
<div class="container">
  
  <!-- Sidebar -->
  <aside class="sidebar">
    <ul class="menu">
      <li class="menu__option">
        <input class="search-box" type="search" placeholder="Search" />
      </li>
      <li class="menu__option menu--selected">
        <button>All Tasks</button>
      </li>
      <li class="menu__option">
        <button>Complete</button>
      </li>
      <li class="menu__option">
        <button>Incomplete</button>
      </li>
      <li class="menu__option menu__new-task">
        <button>+ New Task</button>
      </li>
    </ul>
  </aside>
  
  <!-- Content -->
  <main class="body">
  
    <!-- Form -->
    <form class="form">
      <input class="task__name" name="taskName" type="text" placeholder="Task" />
      <input class="task__save" type="submit" value="Save"/>
    </form>
    <p class="error">Input should not be empty.</p>
    
    <!-- Tasks -->
    <h1>Today</h1>
    <time>...</time>
    <hr />
    <h2>Tasks</h2>
    <ul class="taskdeck">
      <li class="taskcard taskcard--completed">
        <input type="checkbox" />
        <span>Task name</span>
      </li>
    </ul>
  </main>
</div>
```

