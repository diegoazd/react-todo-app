import React, { Component } from 'react';
import Sidebar from './components/sidebar/Sidebar';
import Content from './components/content/Content';
import uuidv4 from 'uuid/v4';

import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {tasks: [], isHiddenTaskBox: true};
  }

  addTask = (taskName) => {
    const task = {id: uuidv4(), name: taskName, completed: false};
    this.setState({tasks: [...this.state.tasks, task], isHiddenTaskBox: true})
  }

  completeTask = (id) => {
    this.setState({tasks: this.mergeTask(this.state.tasks, id)});
  }

  mergeTask = (tasks, id) => {
    for(var i=0; i < tasks.length; i++) {
      if(tasks[i].id === id) {
        tasks[i] = Object.assign({}, tasks[i], {completed: !tasks[i].completed});
        break;
      }
    }

    return tasks;
  }

  showTaskBar = () => {
    this.setState({isHiddenTaskBox: false})
  }

  render() {
    return (
      <div className="container">
        <Sidebar showTaskBar={this.showTaskBar}/>
        <Content tasks={this.state.tasks} addTask={this.addTask} isHiddenTaskBox= {this.state.isHiddenTaskBox} completeTask={this.completeTask}/>
      </div>
    );
  }
}

export default App;
