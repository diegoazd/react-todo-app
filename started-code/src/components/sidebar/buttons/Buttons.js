import React, { Component, Fragment } from 'react';

class Button extends Component {
  render() {
    return (
      <Fragment>
        <li className="menu__option menu--selected">
          <button>All Tasks</button>
        </li>
        <li className="menu__option">
          <button>Complete</button>
        </li>
        <li className="menu__option">
          <button>Incomplete</button>
        </li>
        <li className="menu__option menu__new-task">
          <button onClick={this.props.showTaskBar}>+ New Task</button>
        </li>
      </Fragment>
    );
  }
}

export default Button;