import React, { Component } from 'react';

class Search extends Component {

  render() {
    return (
      <input className="search-box" type="search" placeholder="Search" />
    );
  }
}

export default Search;