import React, { Component } from 'react';
import Search from './search/Search';
import Buttons from './buttons/Buttons';

class Sidebar extends Component {
  render() {
    return (
      <aside className="sidebar">
       <ul className="menu">
         <li className="menu__option">
         <Search />
         </li>
         <Buttons showTaskBar={this.props.showTaskBar}/>
       </ul>
     </aside>
    );
  }  
}

export default Sidebar;