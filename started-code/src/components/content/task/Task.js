import React, { Component, Fragment } from 'react';

class Task extends Component {

  constructor() {
    super();
    this.state= {isHiddenError: true}
  }

  addTask = (e) => {
    e.preventDefault();
    e.target.taskName.value ? this.props.addTask(e.target.taskName.value) : this.setState({isHiddenError: false});
  }

  render() {
    return (
      <Fragment>
        <form className="form" name="taskForm" onSubmit={this.addTask}>
          <input className="task__name" name="taskName" type="text" placeholder="Task" />
          <input className="task__save" type="submit" value="Save" />
        </form>
        {!this.state.isHiddenError && <p className="error">Input should not be empty.</p>}
      </Fragment>
    );
  }
}

export default Task;
