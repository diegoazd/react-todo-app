import React, { Component } from 'react';
import Task from './task/Task';
import TaskList from './task-list/TaskList'

class Content extends Component {
  render() {
    return (
      <main className="body">
        {!this.props.isHiddenTaskBox && <Task addTask={this.props.addTask}/>}
        <h1>Today</h1>
        <time>{new Date().toDateString()}</time>
        <hr />
        <TaskList tasks={this.props.tasks} completeTask={this.props.completeTask}/>
      </main>
    );
  }
}

export default Content;