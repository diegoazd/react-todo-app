import React, { Component, Fragment } from 'react';
import TaskLi from './TaskLi';

class TaskList extends Component {
  render() {
    return (
      <Fragment>
        <h2>Tasks</h2>
        <ul className="taskdeck">
            {this.props.tasks.map((e) => <TaskLi key={e.id} name={e.name} completed={e.completed} taskId={e.id} completeTask={this.props.completeTask}/> )}
        </ul>
      </Fragment>
    );
  }
}

export default TaskList;