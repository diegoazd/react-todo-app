import React, { Component } from 'react';

class TaskLi extends Component {
  
  render() {
    return(
      <li className={this.props.completed ? 'taskcard taskcard--completed' : 'taskcard taskcard'}>
        <input type="checkbox" defaultChecked={this.props.completed} onChange={() => this.props.completeTask(this.props.taskId)}/>
        <span>{this.props.name}</span>
      </li>
    );
  }
}

export default TaskLi;